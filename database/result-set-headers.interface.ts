export interface IResultSetHeaders {
  affectedRows: number
  fieldCount: number
  info: string
  insertId: number
  serverStatus: number
  warningStatus: number
}
