import { IResultSetHeaders } from './result-set-headers.interface'

export interface IQueryResult {
  count: number
  rows: any[]
  headers: IResultSetHeaders
  columns?: string[]
}
