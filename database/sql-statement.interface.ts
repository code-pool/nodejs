export interface ISqlStatement {
  sql: string
  values?: any[]
}
