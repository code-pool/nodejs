import { DatabaseWithoutPool } from './lib/database-without-pool'
import { DatabaseWithPool } from './lib/database-with-pool'
import { IDatabase } from './database.interface'
import { IDatabaseOptions } from './database-options.interface'

const debug = require('debug')('code-pool:database:DatabaseFactory')

export class DatabaseFactory {
  public static configDefaults: IDatabaseOptions = {
    host: '',
    user: '',
    password: '',
    database: '',
    //
    connectionLimit: 100,
    charset: 'utf8mb4_general_ci',
    supportBigNumbers: true,
    bigNumberStrings: true,
    //
    usePool: false,
  }

  public static create = (options: IDatabaseOptions): IDatabase => {
    options = {...DatabaseFactory.configDefaults, ...options}
    const usePool: boolean = !!options.usePool

    // Avoid this warning my MySQL2:
    //
    // Ignoring invalid configuration option passed to Connection: usePool.
    // This is currently a warning, but in future versions of MySQL2,
    // an error will be thrown
    // if you pass an invalid configuration option to a Connection
    delete options.usePool

    if (usePool) {
      debug(`new DatabaseWithPool(`, options, `)`)
      return new DatabaseWithPool(options)
    } else {
      debug(`new DatabaseWithoutPool(`, options, `)`)
      return new DatabaseWithoutPool(options)
    }
  }
}

