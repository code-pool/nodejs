# Database Common 2.0

Common TypeScript library for database access.

## General Use

* Use `DatabaseFactor.create()` for creating a database instance.
* Perform queries with `query()`.
* Execute procedures with `execute()`.

### Connections

In general,
one does not bother with `connect()` and `release()`
as these are managed internally by the `query()` and `execute()` methods.

For multi-statement execution,
or where need dictates explicit control of connections,

* Get a connection with `connect()`.
* Use `queryWithConnection()` and `executeWithConnection()`.
* Release the connection with `release()`.


