import { ISqlStatement } from './sql-statement.interface'
import { IQueryResult } from './query-result.interface'

/**
 * Purpose:
 *
 * The extra work done is to produce uniform `IQueryResult` results
 * since the raw queries produce results of different formats.
 */
export interface IDatabase {
  query: (statement: ISqlStatement) => Promise<IQueryResult>
  execute: (statement: ISqlStatement) => Promise<void>
}
