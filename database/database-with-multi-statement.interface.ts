import { ISqlStatement } from './sql-statement.interface'
import { IQueryResult } from './query-result.interface'
import { IResultSetHeaders } from './result-set-headers.interface'

export interface IDatabaseMultiStatement {
  ////////////////////////////////////////
  // Only use these with multi-statement queries.
  // Be sure to trap errors and release connections properly.
  ////////////////////////////////////////
  connect: () => Promise<any>
  release: (connection: any, force?: boolean) => Promise<void>

  queryWithConnection: (connection: any, statement: ISqlStatement) => Promise<IQueryResult>
  executeWithConnection: (connection: any, statement: ISqlStatement) => Promise<IResultSetHeaders>
}
