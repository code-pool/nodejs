export interface IDatabaseOptions {
  host: string
  port?: number
  user: string
  password?: string
  database: string

  ////////////////////////////////////////
  // Generally not needed.
  ////////////////////////////////////////

  charset?: string
  supportBigNumbers?: boolean
  bigNumberStrings?: boolean

  /**
   *
   * This isn't a property for the underlying database connections.
   * This controls whether the factory uses a database pool.
   * It defaults to `true`.
   */
  usePool?: boolean

  /**
   * Pooling support.
   */
  waitForConnections?: boolean
  connectionLimit?: number
}

