import { IQueryResult } from '../query-result.interface'
import { IResultSetHeaders } from '../result-set-headers.interface'
import { ISqlStatement } from '../sql-statement.interface'

const debug = require('debug')('code-pool:database:lib:BaseDatabase')

export abstract class BaseDatabase {

  /////////////////////////////////////////////////////////////////////////////
  // Connections
  /////////////////////////////////////////////////////////////////////////////

  /**
   * Obtain a new connection.
   */
  public abstract connect(): Promise<any>

  /**
   * Release a connection.
   *
   * @param connection
   * @param force - Request a forcible disconnection.
   */
  public abstract release(
    connection: any,
    force?: boolean,
  ): Promise<void>

  /////////////////////////////////////////////////////////////////////////////
  // Queries
  /////////////////////////////////////////////////////////////////////////////

  /**
   * Perform a query.
   * This converts the result into a uniform `IQueryResult` POJO.
   *
   * This handles connections automatically.
   *
   * @param statement
   */
  query = async (statement: ISqlStatement): Promise<IQueryResult> => {
    const connection = await this.connect()

    let result
    try {
      this.debugStatement('Executing query', statement)
      result = this.reformatQueryResults(
        await connection.query(statement.sql, statement.values),
      )
    } catch (e) {
      debug('Caught exception from database ', e)
      throw this.preserveSqlStatement(e, statement)
    } finally {
      await this.release(connection)
    }

    return result
  }

  /**
   * Execute a statement.
   *
   * This handles connections automatically.
   *
   * @param statement
   */
  execute = async (statement: ISqlStatement): Promise<void> => {
    const connection = await this.connect()

    try {
      this.debugStatement('Executing statement', statement)
      await connection.execute(statement.sql, statement.values)
    } catch (e) {
      debug('Caught exception from database ', e)
      throw this.preserveSqlStatement(e, statement)
    } finally {
      await this.release(connection)
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // Multi-Statement Support
  /////////////////////////////////////////////////////////////////////////////

  /**
   * This was created for multi-statement support.
   *
   * @param connection
   * @param statement
   */
  queryWithConnection = async (
    connection: any,
    statement: ISqlStatement,
  ): Promise<IQueryResult> => {
    this.debugStatement('Running query ', statement)
    return this.reformatQueryResults(await connection.execute(statement))
  }

  /**
   * This was created for multi-statement support.
   *
   * @param connection
   * @param statement
   */
  executeWithConnection = async (
    connection: any,
    statement: ISqlStatement,
  ): Promise<IResultSetHeaders> => {
    this.debugStatement('Executing ', statement)
    return this.reformatExecuteResults(await connection.execute(statement))
  }

  /////////////////////////////////////////////////////////////////////////////
  // Protected methods
  /////////////////////////////////////////////////////////////////////////////

  protected reformatExecuteResults = async (
    rawResult: any,
  ): Promise<IResultSetHeaders> => {
    const [r] = rawResult
    debug('Result Set Headers ', r)
    return r
  }

  protected reformatQueryResults = async (
    rawResult: any,
  ): Promise<IQueryResult> => {
    const [r, v] = rawResult
    if (Array.isArray(r)) {
      if (Array.isArray(r[0]))
        return {
          count: r[0].length,
          rows: r[0],
          headers: r[1],
          columns: v.map((x: any) => x?.name ?? ''),
        }
      else return {
        count: r.length,
        rows: r,
        headers: {} as any,
        columns: v.map((x: any) => x?.name ?? ''),
      }
    } else
      throw new Error('No data returned.')
  }

  protected debugStatement = (message: string, statement: ISqlStatement) => {
    const MaxSqlDebugLength = 2048

    const copy = {
      sql: statement.sql,
      values: [] as any[],
    }

    if (Array.isArray(statement.values))
      copy.values = Array.from(statement.values)

    if (copy.sql.length > MaxSqlDebugLength) {
      copy.sql = '[SQL TOO LONG]'
    }

    for (let i = 0; i < copy.values.length; i++) {
      const value = copy.values[i]
      if (typeof value === 'string' && value.length > MaxSqlDebugLength) {
        copy.values[i] = '[SQL TOO LONG]'
      }
    }

    debug(message, copy)
  }

  protected preserveSqlStatement = (
    error: Error,
    statement: ISqlStatement,
  ): Error => {
    const errorWithJson = error as any

    errorWithJson.errorJson = {
      sql: statement.sql,
      values: Array.isArray(statement.values)
        ? Array.from(statement.values)
        : [],
    }

    return errorWithJson
  }
}
