import { BaseDatabase } from './base-database'
import { IDatabase } from '../database.interface'
import { IDatabaseMultiStatement } from '../database-with-multi-statement.interface'
import { IDatabaseOptions } from '../database-options.interface'

const debug = require('debug')('code-pool:database:lib:DatabaseWithoutPool')
const mysql = require('mysql2/promise')

export class DatabaseWithoutPool
  extends BaseDatabase
  implements IDatabase, IDatabaseMultiStatement {

  constructor(private options: IDatabaseOptions) {
    super()
  }

  public connect = async (): Promise<any> => {
    debug(`Requesting connection.`)
    return mysql.createConnection(this.options)
  }

  public release = async (
    connection: any,
    force = false,
  ): Promise<void> => {
    if (force) {
      debug(`Forcibly releasing connection.`)
      return connection.destroy()
    } else {
      debug(`Releasing connection.`)
      return connection.end()
    }
  }
}
