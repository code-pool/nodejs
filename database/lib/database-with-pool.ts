import { BaseDatabase } from './base-database'
import { IDatabase } from '../database.interface'
import { IDatabaseMultiStatement } from '../database-with-multi-statement.interface'
import { IDatabaseOptions } from '../database-options.interface'

const debug = require('debug')('code-pool:database:lib:DatabaseWithPool')
const mysql = require('mysql2/promise')

export class DatabaseWithPool
  extends BaseDatabase
  implements IDatabase, IDatabaseMultiStatement {

  private readonly options: IDatabaseOptions
  private pool: any

  constructor(options: IDatabaseOptions) {
    super()
    // Extra options for pooling.
    this.options = {
      waitForConnections: true,
      ...options,
    }
    this.pool = mysql.createPool(this.options)
  }

  public connect = async (): Promise<any> => {
    debug(`Requesting connection.`)
    return this.pool.getConnection()
  }

  public release = async (
    connection: any,
  ): Promise<void> => {
    debug(`Releasing connection.`)
    this.pool.releaseConnection(connection)
  }
}
