import { randomBytes } from 'crypto'

export class Bfid {

  public static create(): string {
    return randomBytes(32).toString('hex')
  }

}
